﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matemaatilineHarjutus
{
    class Program
    {
        static void Main(string[] args)
        {
            const int V = 10;
            const int R = 10;
            int veerg = 0;
            int rida = 0;

            int[,] massiiv = new int[V, R]; //massiivi kehtestamine
            Random suva = new Random();
            #region logo
            Console.BackgroundColor = ConsoleColor.Cyan;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
            Console.WriteLine("|w|...Matemaatiline.Harjutus...|w|");
            Console.WriteLine("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            #endregion
            algus:
            #region massiiviTäitmine
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("------Massiivi täitmine:----------");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            #endregion
            for (veerg = 0; veerg < V;) //veeru loendaja
            {
                #region veeruNumber
                Console.BackgroundColor = ConsoleColor.Cyan;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.Write(veerg + 1);
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;
                #endregion

                for (rida = 0; rida < R;) //rea loendaja
                {
                    int väärtus = suva.Next(0, 99);
                    massiiv[veerg, rida] = väärtus;
                    Console.Write($"\t{massiiv[veerg, rida]}; ");
                    rida++;
                }
                Console.WriteLine();
                veerg++;
            }
            #region massiivOnTäidetud
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine("-------Massiiv on täidetud!-------");
            Console.BackgroundColor = ConsoleColor.Black;
            #endregion
            #region massiiviLoendur
            int count = 0;
            foreach (int element in massiiv)
            {
                count++;
            }
            #endregion
            #region massiiviLoenduriAruanne
            Console.BackgroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"Massiivis on: {count} ühikut");
            #endregion
            #region uusTäitmisePäring
            Console.WriteLine("Kas genereering uue massiivi? (Y/N)");
            string uuesti = Console.ReadLine();
            if (uuesti == "y" || uuesti == "yes")
            {
                goto algus;
            }
            else
            {
            }
            #endregion
            otsing:
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("Otsi massiivist numbrit(0-99):");
            int query = int.Parse(Console.ReadLine());
            if (query >= 0 && query < 100)
            {
                count = 0;
                Console.BackgroundColor = ConsoleColor.Green;
                Console.ForegroundColor = ConsoleColor.White;

                for (veerg = 0; veerg < V;)
                {
                    for (rida = 0; rida < R;)
                    {
                        if (massiiv[veerg, rida] == query)
                        {
                            int asukohtVeerg = veerg + 1;
                            int asukohtRida = rida + 1;
                            Console.WriteLine($"Numbri asukoht massiivis on [{asukohtVeerg},{asukohtRida}].");
                            count++;
                        }
                        else
                        {
                        }
                        rida++;
                    }
                    veerg++;
                }
                Console.WriteLine($"Leitud {count} vastet!");
            }
            
        
            else
            {
                goto otsing;
            }

            goto otsing;
        }
    }
}
